#!/bin/bash
# NAME:         print_20minutes_jeux
# VERSION:      0.2
# AUTHOR:       Guilhem Marion (based on extract_pdf_pages by Glutanimate)
# DESCRIPTION:  Prints "games" page from today's 20minutes
# FEATURES:     
# DEPENDENCIES: pdfgrep pdftk
#               Pdftk is not in apt repos on Ubuntu any more. You shall install it from snaps
#               However, this means you cannot use /tmp due to containment policies.
#
# LICENSE:      GNU GPLv3 (http://www.gnu.de/documents/gpl-3.0.en.html)
#
# NOTICE:       THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. 
#               EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES 
#               PROVIDE THE PROGRAM “AS IS” WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR 
#               IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
#               AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND 
#               PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE,
#               YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
#
#               IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING WILL ANY 
#               COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR CONVEYS THE PROGRAM AS 
#               PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, 
#               INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE 
#               THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED 
#               INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE OF THE 
#               PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS), EVEN IF SUCH HOLDER OR OTHER 
#               PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
#
# USAGE:        ./print_20minutes_jeux.sh

#STRING="$1"
#FILE="$2"
STRING="Cancer du 22 juin au 22 juillet"
FILE="20min.pdf"
FILENAME="${FILE##*/})"
BASENAME="${FILENAME%.*}"
PDF_URL=`date +http://pdf.20mn.fr/%Y/quotidien/%Y%m%d_TOU.pdf`

PRINTER=""

# Uncomment to print every command executed
# set -x
set -e

echo "Fetching 20minutes..."

wget -O "$FILE" -q "$PDF_URL"

echo "Processing $FILE..."

## find pages that contain string, remove duplicates, convert newlines to spaces

echo "Looking for $STRING..."

PAGES="$(pdfgrep -n "$STRING" "$FILE" | cut -f1 -d ":" | uniq | tr '\n' ' ')"

#echo "Matching pages:
#$PAGES"

## extract pages to new file in original directory

echo "Extracting result pages..."

pdftk "$FILE" cat $PAGES output "${BASENAME}_jeux.pdf"

echo "Printing on $PRINTER..."

lp -d "$PRINTER" "$FILE"

echo "Cleanup..."

rm "$FILE" "${BASENAME}_jeux.pdf"

echo "Done."
