# Print20minutes_jeux 

Print20minutes_jeux is a shell script for people who are too busy to plan 
how they are wasting their time. It prints 20minutes' (a french free newspaper 
with little journalistic value but great games and horoscope) games page.

## Requirements

This is a shell script, that requires the `lp` program, `pdftk` and `pdfgrep`. 
See the script's documentation for more information. 
## Usage

Add your printer's name in the script, then
```
sh ./print_20minutes_jeux.sh
```

## Acknowledgements

The author does not endorse use of this script during work hours. Use at your 
own (professional) risk.

This script is based on [extract_pdf_pages 0.1](http://askubuntu.com/questions/454934/how-can-i-extract-pages-containing-a-given-string-from-a-pdf-file)
by Glutanimate.
